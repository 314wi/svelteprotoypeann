import { writable } from 'svelte/store';

export interface ListItem {
    id?: number;
    name: string;
    price: number;
    crossed?: boolean;
  }

let shoppingList = new Map([
    [0, { name: "Item 1", price: 10.99, crossed: false }],
    [1, { name: "Item 2", price: 5.49, crossed: false }],
    [2, { name: "Item 3", price: 7.99, crossed: false }],
]);


export const jsonStore = writable(shoppingList);

export function getItem(itemId: number) {
    const storeValue = get(jsonStore);
    return storeValue.get(itemId);
} 

export function addNewItem(newItem: ListItem) {
    jsonStore.update(curr => {
        const newId = Math.max(...curr.keys()) + 1;
        curr.set(newId, { id: newId, ...newItem, crossed: false });
        return new Map(curr);
    });
}

export function deleteItem(itemId: number) {
    jsonStore.update(curr => {
        curr.delete(itemId);
        return new Map(curr);
    });
}

export function switchCrossedOut(itemId: number) {
    jsonStore.update(curr => {
        const item = curr.get(itemId);
        if (item) {
            curr.set(itemId, { ...item, crossed: !item.crossed });
        }
        return new Map(curr);
    });
}
